import request  from '../utils/request'

export function getStaticData(){
    return request({
        url:'/reports/type/1',
        method:'get',
    })
}