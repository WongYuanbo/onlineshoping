import request  from '../utils/request'

export function getGoodsData(data){
    return request({
        url:'/categories',
        method:'get',
        params:data,
    })
}
export function AddGoodsCateData(data){
    return request({
        url:'/categories',
        method:'post',
        data:data,
    })
}
// 获取参数列表`/users/${data.id}/state/${data.state}`,
export function getPropList(data){
    return request({
        url:`/categories/${data.id}/attributes`,
        method:'get',
        params:{sel:data.sel},
    })
}
//添加动态参数或者静态属性
export function AddPropValue(data){
    return request({
        url:`/categories/${data.id}/attributes`,
        method:'post',
        data:{attr_name:data.attr_name,attr_sel:data.attr_sel},
    })
}
// 修改参数标签
export function AddPropTagData(data){
    return request({
        url:`/categories/${data.id}/attributes/${data.attrId}`,
        method:'put',
       data:{attr_name:data.attr_name,attr_sel:data.attr_sel,attr_vals:data.attr_vals},
    })
}
// 后去商品列表数据
export function getGoodsListData(data){
    return request({
        url:'/goods',
        method:'get',
        params:data,
    })
}
