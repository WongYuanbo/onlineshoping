import { createRouter, createWebHistory } from 'vue-router'
/* Layout */
import Home from '../views/Home/index.vue'

const routes = [
  {
    path: '/',
    redirect:'/login',
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login/index.vue')
  },
  {
    path:'/home',
    name:'home',
    component: Home,
    redirect:'/welcome',
    children:[
      {
        path:'/welcome',
        component:()=>import('../views/Home/Welcome/index.vue'),
      },
      {
        path:'/users',
        component: ()=>import('../views/UserManage/UserManage.vue'), 
      },
      {
        path:'/rights',
        component:()=>import('../views/Permission/PermissionList.vue')
      },
      {
        path:'/roles',
        component:()=>import('../views/Permission/Roles.vue')
      },
      {
        path:'/categories',
        component:()=>import('../views/GoodsManage/goodsCate.vue')
      },
      {
        path:'/params',
        component:()=>import('../views/GoodsManage/CategoryParamMana.vue')
      },
      {
        path:'/goods',
        component:()=>import('../views/GoodsManage/GoodList.vue')
      },
      {
        path:'/goods/add',
        component:()=>import('../views/GoodsManage/GoodsAdd.vue')
      },
      {
        path:'/orders',
        component:()=>import('../views/OrderManger/OrderList.vue')
      },
      {
        path:'/reports',
        component:()=>import('../views/Report/Report.vue')
      },
    ]
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
