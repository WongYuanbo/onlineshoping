
import request  from '../utils/request'

let serve ='/'

// 重置数据库数据
// http://www.ysqorz.top:8091/vueshop/reset,

export function login(data){
    return request({
        url:'/login',
        method:'post',
        data:data
    })
}
export function getMenuData(){
    return request({
        url:'/menus',
        method:'get',
    })
}
export function getUsersData(data){
    return request({
        url:'/users',
        method:'get',
        params:data,
    })
}
export function modifyUsersState(data){
    return request({
        url:`/users/${data.id}/state/${data.state}`,
        method:'put',
    })
}
export function addUsersData(data){
    return request({
        url:'/users',
        method:'post',
        data:data,
    })
}
export function getUsersDataById(data){
    return request({
        url:`/users/${data.id}`,
        method:'get',
    })
}
export function editUsersData(data){
    return request({
        url:`/users/${data.id}`,
        method:'put',
        data:{email:data.email,mobile:data.mobile}
    })
}
export function deleteUsersData(data){
    return request({
        url:`/users/${data.id}`,
        method:'delete',
        data
    })
}
// 获取权限数据
export function getPermissionList(data){
    return request({
        url:`/rights/${data.type}`,
        method:'get',
    })
}
// 获取角色权限数据
export function getRolesList(){
    return request({
        url:`/roles/`,
        method:'get',
    })
}
// 删除角色指定权限
export function deleteRolesRight(data){
    return request({
        url:`/roles/${data.roleId}/rights/${data.rightId}`,
        method:'delete',
    })
}
//设定角色权限
export function setRolesRight(data){
    return request({
        url:`/roles/${data.roleId}/rights`,
        method:'post',
        data:{rids:data.rids}
    })
}
// 设置用户的角色
export function setUsersRole(data){
    return request({
        url:`/users/${data.userId}/role`,
        method:'put',
        data:{rid:data.rId}
    })
}




