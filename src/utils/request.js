import router from '@/router';
import axios from 'axios';
import {getToken} from './auth' 
const service = axios.create(
    {
        // baseURL:"http://127.0.0.1:8888/api/private/v1",
        baseURL:'http://www.ysqorz.top:8888/api/private/v1/',
        timeout:3000
    }
)

// 请求拦截
service.interceptors.request.use(
     config =>{
         config.headers.Authorization  =  getToken();
         return config;
     }
)

//响应拦截
service.interceptors.response.use(
   response =>{
       return response;
   }
)

export default service;