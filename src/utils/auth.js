
import Cookies from 'js-cookie'

export  const TokenKey = 'adminToken'
export function getToken() {
    // return Cookies.get(TokenKey)
    return getSessionStorage(TokenKey)
  }
export function setSessionStorage(key,value){
    return window.sessionStorage.setItem(key,JSON.stringify(value));
}
export function setLocalStorage(key,value){
    return window.localStorage.setItem(key,JSON.stringify(value));
}

export function getSessionStorage(key){
    return JSON.parse(window.sessionStorage.getItem(key))
}
export function getLocalStorage(key){
    return JSON.parse(window.localStorage.getItem(key))
}
export function removeSessionStorage(key){
    return window.sessionStorage.removeItem(key);
}

//转换为指定时间格式
export function formatDate(date, fmt) {
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
    };
    for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        let str = o[k] + '';
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str));
      }
    }
    return fmt;
  };
  
  function padLeftZero (str) {
    return ('00' + str).substr(str.length);
  };