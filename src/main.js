import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/zh-CN'
// 导入全局样式表
import '@/assets/css/global.css'
import '@/assets/iconFont/iconfont.css'
// 导入路由全局守卫
import '@/permission'

// 
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
// 导入echarts
import * as echarts from 'echarts'

const app = createApp(App);
app.use(ElementPlus,{locale})
app.use(store)
app.use(router)
app.use(VueQuillEditor)
app.config.globalProperties.$echarts= echarts
app.mount('#app')
