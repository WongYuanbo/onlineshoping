import request  from '../utils/request'

export function getOrdersListData(data){
    return request({
        url:'/orders',
        method:'get',
        params:data,
    })
}
export function getOrderExpressData(){
    return request({
        url:'/kuaidi/804909574412544580',
        method:'get',
    })
}

