import router from './router'
import {getToken} from '@/utils/auth'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList =['/login'] //不重定向白名单

router.beforeEach((to,from,next) =>{
     //开启进度条
    NProgress.start()
     if(getToken()){
        //  如果有token值，且要访问登陆界面时，直接跳转到默认首页
         if(to.path === '/login'){
            next({path:'/'})
            NProgress.done() 
         }
         else
         {
             next();
             NProgress.done()
         }
     }
     else
     {   
        //  如果是去往白名单包含路径则直接去
         if(whiteList.indexOf(to.path) !== -1){
             next()
             NProgress.done()
         }
         else
         {
             next(`/login?redirect=${to.path}`) //否则全部重定向到登陆页
             NProgress.done()
         }
     }
 })
