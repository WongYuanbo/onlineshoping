/**
 * Created by jiachenpan on 16/11/18.
 */

export function isvalidUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

export function isExternal(path) {
  // 判断字符串是否是以“https 、mailto或tel开头
  return /^(https?:|mailto:|tel:)/.test(path)
}
